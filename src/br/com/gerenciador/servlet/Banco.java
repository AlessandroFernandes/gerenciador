package br.com.gerenciador.servlet;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Banco {

	public static List<Empresa> ListaEmpresa = new ArrayList<>();
	
	static {
		Empresa empresa1 = new Empresa();
		empresa1.setNome("AASP");
		empresa1.setId(2019);
		
		Empresa empresa2 = new Empresa();
		empresa2.setNome("Itau");
		empresa2.setId(2020);
		
		Banco.ListaEmpresa.add(empresa1);
		Banco.ListaEmpresa.add(empresa2);
	}
	
	public void AdicionarEmpresa(Empresa empresa) {
		Banco.ListaEmpresa.add(empresa);
	}
	
	public List<Empresa> getEmpresas(){
		return Banco.ListaEmpresa;
	}
	
	public void DeletarEmpresa(Integer idEmpresa) {
		Iterator<Empresa> it = ListaEmpresa.iterator();
		
		while(it.hasNext()) {
			Empresa em = it.next();
			
			if(em.getId() == idEmpresa) {
				it.remove();
			}
		}
	}
}
