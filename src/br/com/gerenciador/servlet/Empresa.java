package br.com.gerenciador.servlet;

import java.util.Date;

public class Empresa {

	private String Nome;
	private int Id;
	private Date Date = new Date();
	
	public Date getDate() {
		return Date;
	}

	public void setDate(Date date) {
		Date = date;
	}

	public String getNome() {
		return Nome;
	}
	
	public void setNome(String nome) {
		Nome = nome;
	}
	
	public int getId() {
		return Id;
	}
	
	public void setId(int id) {
		Id = id;
	}
	
}
