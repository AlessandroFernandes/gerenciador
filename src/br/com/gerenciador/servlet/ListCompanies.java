package br.com.gerenciador.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/listCompanies")
public class ListCompanies extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Banco banco = new Banco();
		List<Empresa> empresas = banco.getEmpresas();
		
		response.getWriter().append("Served at: ").append(request.getContextPath());
		RequestDispatcher rq = request.getRequestDispatcher("listarEmpresas.jsp");
		request.setAttribute("listCompanies", empresas);
		rq.forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String idInput = request.getParameter("idCompany");
		int idCompany = Integer.parseInt(idInput);
		System.out.println(idCompany);
		Banco banco = new Banco();
		banco.DeletarEmpresa(idCompany);

		response.sendRedirect("listCompanies");
	}

}	
