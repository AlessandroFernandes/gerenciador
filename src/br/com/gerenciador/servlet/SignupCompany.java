package br.com.gerenciador.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/SignupCompany")
public class SignupCompany extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		String empresaInput = request.getParameter("name");
		String dataInput = request.getParameter("date");
		int idInput = (int)Math.random();
		Date date = null;
		
		try 
		{
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			date = sdf.parse(dataInput);
		}
		catch(Exception ex)
		{
			throw new ServletException(ex);
		}
		
		Empresa empresa = new Empresa();
		empresa.setNome(empresaInput);
		empresa.setDate(date);
		empresa.setId(idInput);
		
		Banco banco = new Banco();
		banco.AdicionarEmpresa(empresa);
	
		RequestDispatcher rq = request.getRequestDispatcher("novaEmpresaCriada.jsp");
		request.setAttribute("company", empresa.getNome());
		rq.forward(request, response);
		
	}

}
