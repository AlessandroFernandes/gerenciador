<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:url value="/listCompanies" var="listarEmpresa" ></c:url>

<!DOCTYPE html>

<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>List companies</title>
		<link type="text/css" rel="stylesheet" href="css/test.css"/>
	</head>
	<body>
		<h3 class="test">List companies:</h3>
		<ul>
			
			<c:forEach items="${ listCompanies }" var="company">
				<li>${ company.nome } 
					<fmt:formatDate value="${ company.date }"/> 
					&nbsp; 
					<form action="${ listarEmpresa }" method="POST">
						<input type="text" name="idCompany" value="${ company.id }" />
						<button type="submit">Delete</button>
					</form>
				</li>

			</c:forEach>	
		</ul>
	</body>
</html>