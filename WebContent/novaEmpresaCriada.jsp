<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Nova empresa criada</title>
	</head>
	
	<body>
		<c:if test="${not empty company}">
			<p>Company created: ${ company }</p>
		</c:if>
		
		<c:if test="${empty company}">
			<p>There are no companies</p>
		</c:if>
	</body>
</html>