<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:url value="/SignupCompany" var="signupLink"></c:url>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Company registration form</title>
	</head>
	<body>
		<form method="post" action="${ signupLink }">
		
			<div style="display: block">
				<input type="text" placeholder="Company register" name="name" />
				
			</div>
			
			<div style="display: block">
				<input type="text" name="date" />
			</div>
			
			<input type="submit" value="Register" />
		</form>
	</body>
</html>